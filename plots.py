import glob
import itertools
from multiprocessing import Process

import matplotlib.pyplot as plt
import numpy as np

import algorithm as alg
import data_handling as dh
import measuring


def plot_sequential_fair_experiments():
    k = 30

    maxnum = 201
    minnum = 0

    data_file_name = "FPR_Zafar_N_10000"
    plot_vary_alpha_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], beta=0, k=k,
                               base_method='linear')
    plot_vary_alpha_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], beta=0, k=k,
                               base_method='logistic')

    data_file_name = "FNR_Zafar_N_10000"
    plot_vary_beta_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alpha=0, k=k,
                              base_method='linear')
    plot_vary_beta_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alpha=0, k=k,
                              base_method='logistic')

    data_file_name = "FPR_and_FNR_Zafar_samesign_N_10000"
    plot_vary_alpha_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], beta=0, k=k,
                               base_method='linear')
    plot_vary_alpha_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], beta=0, k=k,
                               base_method='logistic')
    plot_vary_beta_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alpha=0, k=k,
                              base_method='linear')
    plot_vary_beta_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alpha=0, k=k,
                              base_method='logistic')
    plot_vary_alphabeta_experiment(data_file_name=data_file_name, alphabetas=[i / 10 for i in range(1, maxnum)], k=k,
                                   base_method='linear')
    plot_vary_alphabeta_experiment(data_file_name=data_file_name, alphabetas=[i / 10 for i in range(1, maxnum)], k=k,
                                   base_method='logistic')

    maxnum = 101
    data_file_name = "FPR_and_FNR_Zafar_diffsign_N_10000"
    plot_vary_alpha_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], beta=0, k=k, base_method='linear')
    plot_vary_alpha_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], beta=0, k=k,
                               base_method='logistic')
    plot_vary_beta_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alpha=0, k=k,
                               base_method='linear')
    plot_vary_beta_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alpha=0, k=k,
                              base_method='logistic')
    plot_vary_alphabeta_experiment(data_file_name=data_file_name, alphabetas=[i / 10 for i in range(1, maxnum)], k=k,
                              base_method='linear')
    plot_vary_alphabeta_experiment(data_file_name=data_file_name, alphabetas=[i / 10 for i in range(1, maxnum)], k=k,
                                    base_method='logistic')


    plt.show()

def plot_vary_alpha_experiment( alphas, beta, base_method, data_file_name, k=1, N=10000, log_plot=False):
    if data_file_name == "FPR_Zafar_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_sample_data(N)
    elif data_file_name == "FNR_Zafar_N_10000":
        X, t, z = dh.generate_unbalanced_FNR_sample_data(N)
    elif data_file_name == "FPR_and_FNR_Zafar_diffsign_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_and_FNR_diff_sign_sample_data(N)
    elif data_file_name == "FPR_and_FNR_Zafar_samesign_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_and_FNR_same_sign_sample_data(N)
    elif(data_file_name == "compas-scores-two-years-clean"):
        X, t, z = dh.load_data(data_file_name)

    I0 = z < 0.5
    I1 = z > 0.5
    t0 = t[I0]
    t1 = t[I1]

    N0 = t0.shape[0]
    N1 = t1.shape[0]

    print("(n, n0, n1)=" + str((N, N0, N1)))

    I0pos = t0 > 0.5
    I0neg = t0 < 0.5
    I1pos = t1 > 0.5
    I1neg = t1 < 0.5

    X0 = X[I0, :]
    X1 = X[I1, :]

    neg0 = X0[I0neg, :]
    neg1 = X1[I1neg, :]
    pos0 = X0[I0pos, :]
    pos1 = X1[I1pos, :]

    print("sizes of (X0, X1)=" + str((X0.shape[0], X1.shape[0])))

    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=False)
    FNR_diff = []
    FPR_diff = []
    accuracy = []

    for alpha in alphas:
        filename = "models/" + data_file_name + "/" + data_file_name + "_base_method_" + base_method + "_alpha_" + str(alpha) + "_beta_" + str(beta) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
        model = np.load(filename)
        w = model['w']
        FNR0, FPR0 = measuring.get_FNR_and_FPR(X0, t0, w)
        FNR1, FPR1 = measuring.get_FNR_and_FPR(X1, t1, w)

        acc = measuring.get_accuracy(X, t, w)
        FNR_diff.append((FNR0 - FNR1))
        FPR_diff.append((FPR0 - FPR1))
        accuracy.append(acc)


    title = "Accuracy and error differences for varying alpha and beta=" + str(beta) + " with base method " + base_method + " and data file " + data_file_name

    f_errors.suptitle(title)

    fs=10

    axarr_errors[0].plot(alphas, accuracy, 'ks')
    axarr_errors[0].set_ylabel("Accuracy")
    axarr_errors[1].plot(alphas, FPR_diff, 'ro')
    axarr_errors[1].set_ylabel("Error difference")
    axarr_errors[1].plot(alphas, FNR_diff, 'bs')
    axarr_errors[1].set_xlabel("alpha")

    axarr_errors[1].legend(fontsize=fs)
    plt.axhline(axes=axarr_errors[1], color='k')
    if (log_plot):
        axarr_errors[0].set_xscale('log')
        axarr_errors[1].set_xscale('log')

def plot_vary_beta_experiment( alpha, betas, base_method, data_file_name, k=1, N=10000, log_plot=False):

    if data_file_name == "FPR_Zafar_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_sample_data(N)
    elif data_file_name == "FNR_Zafar_N_10000":
        X, t, z = dh.generate_unbalanced_FNR_sample_data(N)
    elif data_file_name == "FPR_and_FNR_Zafar_diffsign_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_and_FNR_diff_sign_sample_data(N)
    elif data_file_name == "FPR_and_FNR_Zafar_samesign_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_and_FNR_same_sign_sample_data(N)
    elif(data_file_name == "compas-scores-two-years-clean"):
        X, t, z = dh.load_data(data_file_name)

    I0 = z < 0.5
    I1 = z > 0.5
    t0 = t[I0]
    t1 = t[I1]

    N0 = t0.shape[0]
    N1 = t1.shape[0]

    print("(n, n0, n1)=" + str((N, N0, N1)))

    I0pos = t0 > 0.5
    I0neg = t0 < 0.5
    I1pos = t1 > 0.5
    I1neg = t1 < 0.5

    X0 = X[I0, :]
    X1 = X[I1, :]

    neg0 = X0[I0neg, :]
    neg1 = X1[I1neg, :]
    pos0 = X0[I0pos, :]
    pos1 = X1[I1pos, :]

    print("sizes of (X0, X1)=" + str((X0.shape[0], X1.shape[0])))

    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=False)
    FNR_diff = []
    FPR_diff = []
    accuracy = []

    for beta in betas:
        filename = "models/" + data_file_name + "/" + data_file_name + "_base_method_" + base_method + "_alpha_" + str(alpha) + "_beta_" + str(beta) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
        model = np.load(filename)
        w = model['w']
        FNR0, FPR0 = measuring.get_FNR_and_FPR(X0, t0, w)
        FNR1, FPR1 = measuring.get_FNR_and_FPR(X1, t1, w)

        acc = measuring.get_accuracy(X, t, w)
        FNR_diff.append(FNR0 - FNR1)
        FPR_diff.append(FPR0 - FPR1)
        accuracy.append(acc)


    title = "Accuracy and error differences for varying beta and alpha=" + str(alpha) + " with base method " + base_method + " " + data_file_name

    f_errors.suptitle(title)
    fs=10

    axarr_errors[0].plot(betas, accuracy, 'ks')
    axarr_errors[0].set_ylabel("Accuracy")
    axarr_errors[1].plot(betas, FPR_diff, 'ro')
    axarr_errors[1].set_ylabel("Error difference")
    axarr_errors[1].plot(betas, FNR_diff, 'bs')
    axarr_errors[1].set_xlabel("beta")

    axarr_errors[1].legend(fontsize=fs)
    plt.axhline(axes=axarr_errors[1], color='k')
    if (log_plot):
        axarr_errors[0].set_xscale('log')
        axarr_errors[1].set_xscale('log')

def plot_vary_alphabeta_experiment( alphabetas, base_method, data_file_name, k=30, N=10000, log_plot=False):
    if data_file_name == "FPR_Zafar_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_sample_data(N)
    elif data_file_name == "FNR_Zafar_N_10000":
        X, t, z = dh.generate_unbalanced_FNR_sample_data(N)
    elif data_file_name == "FPR_and_FNR_Zafar_diffsign_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_and_FNR_diff_sign_sample_data(N)
    elif data_file_name == "FPR_and_FNR_Zafar_samesign_N_10000":
        X, t, z = dh.generate_unbalanced_FPR_and_FNR_same_sign_sample_data(N)
    elif(data_file_name == "compas-scores-two-years-clean"):
        X, t, z = dh.load_data(data_file_name)
    I0 = z < 0.5
    I1 = z > 0.5
    t0 = t[I0]
    t1 = t[I1]

    N0 = t0.shape[0]
    N1 = t1.shape[0]

    print("(n, n0, n1)=" + str((N, N0, N1)))

    I0pos = t0 > 0.5
    I0neg = t0 < 0.5
    I1pos = t1 > 0.5
    I1neg = t1 < 0.5

    X0 = X[I0, :]
    X1 = X[I1, :]

    neg0 = X0[I0neg, :]
    neg1 = X1[I1neg, :]
    pos0 = X0[I0pos, :]
    pos1 = X1[I1pos, :]

    print("sizes of (X0, X1)=" + str((X0.shape[0], X1.shape[0])))

    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=False)
    FNR_diff = []
    FPR_diff = []
    accuracy = []

    for alphabeta in alphabetas:
        filename = "models/" + data_file_name + "/" + data_file_name + "_base_method_" + base_method + "_alpha_" + str(alphabeta) + "_beta_" + str(alphabeta) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
        model = np.load(filename)
        w = model['w']
        FNR0, FPR0 = measuring.get_FNR_and_FPR(X0, t0, w)
        FNR1, FPR1 = measuring.get_FNR_and_FPR(X1, t1, w)

        acc = measuring.get_accuracy(X, t, w)
        FNR_diff.append(FNR0 - FNR1)
        FPR_diff.append(FPR0 - FPR1)
        accuracy.append(acc)


    title = "Accuracy and error differences for varying beta and alpha with base method " + base_method + " " + data_file_name

    #f_errors.suptitle(title)
    fs=10
    plt.rc('text', usetex=True)
    #plt.rc('font', family='serif')
    axarr_errors[0].plot(alphabetas, accuracy, 'kx')
    axarr_errors[0].set_ylabel("Accuracy")
    axarr_errors[1].plot(alphabetas, FPR_diff, 'r+', label="FPR-diff")
    axarr_errors[1].set_ylabel("Signed error difference")
    axarr_errors[1].plot(alphabetas, FNR_diff, 'bx', label="FNR-diff")
    axarr_errors[1].set_xlabel(r"$\alpha=\beta$")
    axarr_errors[1].legend(fontsize=fs, bbox_to_anchor=(1.025, 1), loc=2, borderaxespad=0.)
    plt.axhline(axes=axarr_errors[1], color='gray')

    if (log_plot):
        axarr_errors[0].set_xscale('log')
        axarr_errors[1].set_xscale('log')

def plot_compas_experiments():
    k = 30
    maxnum = 11
    data_file_name = "compas-scores-two-years-clean"
    plot_compas_experiment(data_file_name=data_file_name, alphas=[2.0**i for i in range(0, maxnum)], betas=[0], k=k,
                           base_method='linear')
    plot_compas_experiment(data_file_name=data_file_name, alphas=[2.0**i for i in range(0, maxnum)], betas=[0], k=k,
                           base_method='logistic')
    plot_compas_experiment(data_file_name=data_file_name, betas=[2.0**i for i in range(0, maxnum)], alphas=[0], k=k,
                           base_method='linear')
    plot_compas_experiment(data_file_name=data_file_name, betas=[2.0**i for i in range(0, maxnum)], alphas=[0], k=k,
                           base_method='logistic')
    plot_compas_experiment(data_file_name=data_file_name, alphas=[2.0**i for i in range(1, maxnum)],
                           betas=[2.0**i for i in range(1, maxnum)], k=k,
                           base_method='linear')
    plot_compas_experiment(data_file_name=data_file_name, alphas=[2.0**i for i in range(1, maxnum)],
                           betas=[2.0**i for i in range(1, maxnum)], k=k,
                           base_method='logistic')
    """plot_compas_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], betas=[0], k=k,
                               base_method='linear')
    plot_compas_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(0, maxnum)], betas=[0], k=k,
                               base_method='logistic')
    plot_compas_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alphas=[0], k=k,
                              base_method='linear')
    plot_compas_experiment(data_file_name=data_file_name, betas=[i / 10 for i in range(0, maxnum)], alphas=[0], k=k,
                              base_method='logistic')
    plot_compas_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(1, maxnum)], betas=[i / 10 for i in range(1, maxnum)], k=k,
                                   base_method='linear')
    plot_compas_experiment(data_file_name=data_file_name, alphas=[i / 10 for i in range(1, maxnum)], betas=[i / 10 for i in range(1, maxnum)], k=k,
                                   base_method='logistic')"""

def plot_compas_experiment( alphas, betas, base_method, data_file_name, k=30, N=10000):
    X_orig, t_orig, z_orig = dh.load_data(data_file_name)
    five_fold_indices = np.load("data/" + data_file_name + "_5_fold_indices.npz")

    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=False)
    FNR_diff = []
    FPR_diff = []
    accuracy = []

    if (len(alphas) == 1):

        for beta in betas:
            next_accuracy = 0
            next_FNR_diff = 0
            next_FPR_diff = 0
            for fold in range(0,5):
                N = five_fold_indices['train_indices'][fold].shape[0]
                X_test = X_orig[five_fold_indices['test_indices'][fold]]
                t_test = t_orig[five_fold_indices['test_indices'][fold]]
                z_test = z_orig[five_fold_indices['test_indices'][fold]]
                I0 = z_test < 0.5
                I1 = z_test > 0.5
                t0_test = t_test[I0]
                t1_test = t_test[I1]

                N0 = t0_test.shape[0]
                N1 = t1_test.shape[0]

                print("(n, n0, n1)=" + str((N, N0, N1)))

                X0_test = X_test[I0, :]
                X1_test = X_test[I1, :]

                print("sizes of (X0, X1)=" + str((X0_test.shape[0], X1_test.shape[0])))
                filename = "models/" + data_file_name + "_fold_"+ str(fold) + "/" + data_file_name + "_fold_" + str(fold) + "_base_method_" + base_method + "_alpha_" + str(alphas[0]) + "_beta_" + str(beta) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
                model = np.load(filename)
                w = model['w']
                FNR0, FPR0 = measuring.get_FNR_and_FPR(X0_test, t0_test, w)
                FNR1, FPR1 = measuring.get_FNR_and_FPR(X1_test, t1_test, w)

                next_accuracy += measuring.get_accuracy(X_test, t_test, w)
                next_FNR_diff += FNR0 - FNR1
                next_FPR_diff += FPR0 - FPR1
            FNR_diff.append(next_FNR_diff/5)
            FPR_diff.append(next_FPR_diff/5)
            accuracy.append(next_accuracy/5)
        title = "Accuracy and error differences for varying beta and alpha=0 with base method " + base_method + " " + data_file_name

        f_errors.suptitle(title)

        axarr_errors[0].plot(betas, accuracy, 'ks')
        axarr_errors[0].set_ylabel("Accuracy")
        axarr_errors[1].plot(betas, FPR_diff, 'ro')
        axarr_errors[1].set_ylabel("Error difference")
        axarr_errors[1].plot(betas, FNR_diff, 'bs')
        axarr_errors[1].set_xlabel("beta")

    elif (len(betas) == 1):

        for alpha in alphas:
            next_accuracy = 0
            next_FNR_diff = 0
            next_FPR_diff = 0
            for fold in range(0,5):
                N = five_fold_indices['train_indices'][fold].shape[0]
                X_test = X_orig[five_fold_indices['train_indices'][fold]]
                t_test = t_orig[five_fold_indices['train_indices'][fold]]
                z_test = z_orig[five_fold_indices['train_indices'][fold]]
                I0 = z_test < 0.5
                I1 = z_test > 0.5
                t0_test = t_test[I0]
                t1_test = t_test[I1]

                N0 = t0_test.shape[0]
                N1 = t1_test.shape[0]

                print("(n, n0, n1)=" + str((N, N0, N1)))

                X0_test = X_test[I0, :]
                X1_test = X_test[I1, :]

                print("sizes of (X0, X1)=" + str((X0_test.shape[0], X1_test.shape[0])))
                filename = "models/" + data_file_name + "_fold_"+ str(fold) + "/" + data_file_name + "_fold_" + str(fold) + "_base_method_" + base_method + "_alpha_" + str(alpha) + "_beta_" + str(betas[0]) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
                model = np.load(filename)
                w = model['w']
                FNR0, FPR0 = measuring.get_FNR_and_FPR(X0_test, t0_test, w)
                FNR1, FPR1 = measuring.get_FNR_and_FPR(X1_test, t1_test, w)

                next_accuracy += measuring.get_accuracy(X_test, t_test, w)
                next_FNR_diff += FNR0 - FNR1
                next_FPR_diff += FPR0 - FPR1
            FNR_diff.append(next_FNR_diff/5)
            FPR_diff.append(next_FPR_diff/5)
            accuracy.append(next_accuracy/5)

        title = "Accuracy and error differences for varying alpha and beta=0 with base method " + base_method + " " + data_file_name

        f_errors.suptitle(title)

        axarr_errors[0].plot(alphas, accuracy, 'ks')
        axarr_errors[0].set_ylabel("Accuracy")
        axarr_errors[1].plot(alphas, FPR_diff, 'ro')
        axarr_errors[1].set_ylabel("Error difference")
        axarr_errors[1].plot(alphas, FNR_diff, 'bs')
        axarr_errors[1].set_xlabel("alpha")

    else:

        for alpha in alphas:
            next_accuracy = 0
            next_FNR_diff = 0
            next_FPR_diff = 0
            for fold in range(0,5):
                N = five_fold_indices['train_indices'][fold].shape[0]
                X_test = X_orig[five_fold_indices['train_indices'][fold]]
                t_test = t_orig[five_fold_indices['train_indices'][fold]]
                z_test = z_orig[five_fold_indices['train_indices'][fold]]
                I0 = z_test < 0.5
                I1 = z_test > 0.5
                t0_test = t_test[I0]
                t1_test = t_test[I1]

                N0 = t0_test.shape[0]
                N1 = t1_test.shape[0]

                print("(n, n0, n1)=" + str((N, N0, N1)))

                X0_test = X_test[I0, :]
                X1_test = X_test[I1, :]

                print("sizes of (X0, X1)=" + str((X0_test.shape[0], X1_test.shape[0])))
                filename = "models/" + data_file_name + "_fold_"+ str(fold) + "/" + data_file_name + "_fold_" + str(fold) + "_base_method_" + base_method + "_alpha_" + str(alpha) + "_beta_" + str(alpha) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
                model = np.load(filename)
                w = model['w']
                FNR0, FPR0 = measuring.get_FNR_and_FPR(X0_test, t0_test, w)
                FNR1, FPR1 = measuring.get_FNR_and_FPR(X1_test, t1_test, w)

                next_accuracy += measuring.get_accuracy(X_test, t_test, w)
                next_FNR_diff += FNR0 - FNR1
                next_FPR_diff += FPR0 - FPR1
            FNR_diff.append(next_FNR_diff/5)
            FPR_diff.append(next_FPR_diff/5)
            accuracy.append(next_accuracy/5)
        title = "Accuracy and error differences for varying alpha and beta with base method " + base_method + " " + data_file_name

        f_errors.suptitle(title)

        axarr_errors[0].plot(alphas, accuracy, 'ks')
        axarr_errors[0].set_ylabel("Accuracy")
        axarr_errors[1].plot(alphas, FPR_diff, 'ro')
        axarr_errors[1].set_ylabel("Error difference")
        axarr_errors[1].plot(alphas, FNR_diff, 'bs')
        axarr_errors[1].set_xlabel("alpha=beta")



def plot_vary_k_experiment( alpha, beta, ks, base_method, data_file_name, N=10000):
    X, t, z = dh.load_data(data_file_name)

    N = t.shape[0]

    I0 = z < 0.5
    I1 = z > 0.5
    t0 = t[I0]
    t1 = t[I1]

    N0 = t0.shape[0]
    N1 = t1.shape[0]

    print("(n, n0, n1)=" + str((N, N0, N1)))

    I0pos = t0 > 0.5
    I0neg = t0 < 0.5
    I1pos = t1 > 0.5
    I1neg = t1 < 0.5

    X0 = X[I0, :]
    X1 = X[I1, :]

    neg0 = X0[I0neg, :]
    neg1 = X1[I1neg, :]
    pos0 = X0[I0pos, :]
    pos1 = X1[I1pos, :]

    print("sizes of (X0, X1)=" + str((X0.shape[0], X1.shape[0])))

    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=False)
    FNR_diff = []
    FPR_diff = []
    accuracy = []

    for k in ks:
        filename = "models/" + data_file_name + "/" + data_file_name + "_base_method_" + base_method + "_alpha_" + str(alpha) + "_beta_" + str(beta) + \
                   "_k_" + str(k) + "_N_" + str(N) + ".npz"
        print("Attempting to load " + filename)
        model = np.load(filename)
        w = model['w']
        FNR0, FPR0 = measuring.get_FNR_and_FPR(X0, t0, w)
        FNR1, FPR1 = measuring.get_FNR_and_FPR(X1, t1, w)
        acc = measuring.get_accuracy(X, t, w)
        FNR_diff.append((FNR0 - FNR1))
        FPR_diff.append((FPR0 - FPR1))
        accuracy.append(acc)

    title = "Accuracy and error differences for varying k with alpha=beta=" + str(alpha) + " and base method " + base_method + " " + data_file_name

    #f_errors.suptitle(title)

    fs = 10

    axarr_errors[0].plot(ks, accuracy, 'kx')
    axarr_errors[0].set_ylabel("Accuracy")
    axarr_errors[1].plot(ks, FPR_diff, 'r+', label="FPR-diff")
    axarr_errors[1].set_ylabel("Signed error difference")
    axarr_errors[1].plot(ks, FNR_diff, 'bx', label="FNR-diff")
    axarr_errors[1].set_xlabel("c")
    axarr_errors[1].legend(fontsize=fs, bbox_to_anchor=(1.025, 1), loc=2, borderaxespad=0.)
    plt.axhline(axes=axarr_errors[1], color='gray')

def plot_convergence_experiment(filename):
    experiment = np.load(filename)
    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=True)
    axarr_errors[0].plot(range(0, 101), experiment['errors'])
    axarr_errors[1].plot(range(1, 101), experiment['improvements'])
    plt.show()

def plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10, scaling="None"):
    f_errors, axarr_errors = plt.subplots(2, 1, sharex=True, sharey=False)
    max_errors = []
    min_errors = []
    for i in range(1,11):
        filename = "experiments/normalization"
        if with_base:
            filename += "_with_base_"
        else:
            filename += "_without_base_"
        filename += data_file_name
        filename += "_base_method_" + base_method
        filename += "_alpha_" + str(alpha) + "_beta_" + str(beta)
        filename += "_scaling_" + scaling
        filename += "_train_size_" + str(i/10)
        filename += ".npz"
        experiment = np.load(filename)
        max_errors.append(max(experiment['base_errors']))
        min_errors.append(min(experiment['base_errors']))

    title = "Base errors for varying n with base method " + base_method
    if with_base:
        title += " with heuristic"
    else:
        title += " without heuristic"
    title += ", scaling " + scaling + " and data " + data_file_name
    f_errors.suptitle(title)
    axarr_errors[0].plot(range(1000, 10001, 1000), max_errors, 'bs-')
    axarr_errors[0].set_ylabel("Maximum base errors")
    axarr_errors[1].plot(range(1000, 10001, 1000), min_errors, 'ro-')
    axarr_errors[1].set_xlabel("N")
    axarr_errors[1].set_ylabel("Minimum base errors")

def plot_normalization_experiments():
    data_file_name = "FPR_and_FNR_Zafar_diffsign_N_10000"
    plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10,
                                        scaling="none")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='logistic', alpha=10, beta=10, k=10,
                                        scaling="none")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10,
                                        scaling="standardize")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='logistic', alpha=10, beta=10, k=10,
                                        scaling="standardize")

    data_file_name = "FPR_and_FNR_Zafar_samesign_N_10000"
    plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10,
                                        scaling="none")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='logistic', alpha=10, beta=10, k=10,
                                        scaling="none")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10,
                                        scaling="standardize")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='logistic', alpha=10, beta=10, k=10,
                                        scaling="standardize")

    data_file_name = "compas-scores-two-years-clean"
    plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10,
                                        scaling="none")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='logistic', alpha=10, beta=10, k=10,
                                        scaling="none")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='linear', alpha=10, beta=10, k=10,
                                        scaling="standardize")
    plot_normalization_experiment(data_file_name, with_base=True, base_method='logistic', alpha=10, beta=10, k=10,
                                        scaling="standardize")

    plt.show()

def plot_boundaries_beta(data_file_name, base_method, alpha, betas, k):
    X, t, z = dh.load_data(data_file_name)

    N = t.shape[0]

    I0 = z < 0.5
    I1 = z > 0.5
    t0 = t[I0]
    t1 = t[I1]

    N0 = t0.shape[0]
    N1 = t1.shape[0]

    print("(n, n0, n1)=" + str((N, N0, N1)))

    I0pos = t0 > 0.5
    I0neg = t0 < 0.5
    I1pos = t1 > 0.5
    I1neg = t1 < 0.5

    X0 = X[I0, :]
    X1 = X[I1, :]

    neg0 = X0[I0neg, :]
    neg1 = X1[I1neg, :]
    pos0 = X0[I0pos, :]
    pos1 = X1[I1pos, :]

    n = len(betas)

    print("sizes of (X0, X1)=" + str((X0.shape[0], X1.shape[0])))

    f_models, axarr_models = plt.subplots(n, sharex=True, sharey=True)
    f_errors, (axarr_errors1, axarr_errors2) = plt.subplots(2, 1, sharex=True, sharey=False)
    accuracy = np.zeros(n)
    FNR_diff = np.zeros(n)
    FPR_diff = np.zeros(n)
    for i in range(0, n):
            axarr_models[i].scatter(neg0[:, 0], neg0[:, 1], s=1, alpha=0.15, color='b')
            axarr_models[i].scatter(neg1[:, 0], neg1[:, 1], s=1, alpha=0.15, color='g')
            axarr_models[i].scatter(pos0[:, 0], pos0[:, 1], s=1, alpha=0.15, color='y')
            axarr_models[i].scatter(pos1[:, 0], pos1[:, 1], s=1, alpha=0.15, color='r')

            filename = "models/" + data_file_name + "/" + data_file_name + "_base_method_" + base_method + "_alpha_" + str(
                alpha) + "_beta_" + str(betas[i]) + "_k_" + str(k) + "_N_" + str(N) + ".npz"
            model = np.load(filename)
            w = model['w']
            FNR0, FPR0 = measuring.get_FNR_and_FPR(X0, t0, w)
            FNR1, FPR1 = measuring.get_FNR_and_FPR(X1, t1, w)

            acc = measuring.get_accuracy(X, t, w)
            FNR_diff[i] = abs(FNR0 - FNR1)
            FPR_diff[i] = abs(FPR0 - FPR1)
            accuracy[i] = acc

            xmin, xmax = axarr_models[i].get_xlim()
            ymin, ymax = axarr_models[i].get_ylim()
            axarr_models[i].set_title(
                "alpha: " + str(model["alpha"]) + ", beta: " + str(model["beta"]))

            def plot_hyperplane(color, coef, intercept):
                def xline(x0):
                    return (-(x0 * coef[0]) - intercept) / coef[1]

                def yline(y0):
                    return (-(y0 * coef[1]) - intercept) / coef[0]

                # plt.plot([max(yline(ymin), xmin), min(yline(ymax), xmax)], [max(xline(xmin),ymin), min(xline(xmax),ymax)],
                #         ls="--", color=color)
                axarr_models[i].plot([xmin, xmax], [xline(xmin), xline(xmax)],
                                                          ls="--", color=color)
                axarr_models[i].set_xlim([ymin, ymax])
                axarr_models[i].set_ylim([ymin, ymax])

            plot_hyperplane('b', model['w'][1:], model['w'][0])
            plot_hyperplane('k', model['w_base'][1:], model['w_base'][0])

    axarr_errors1.plot(betas, accuracy, 'k')
    axarr_errors2.plot(betas, FNR_diff, 'b')
    axarr_errors2.plot(betas, FPR_diff, 'r')

def plot_comparison_to_Zafar_alphabeta(k=30):

    results_zafar = np.load("experiments/results_Zafar/disp_mis_compas_cons_type_4.npz")
    FPR0_zafar = results_zafar["FPR0"]
    FPR1_zafar = results_zafar["FPR1"]
    FNR0_zafar = results_zafar["FNR0"]
    FNR1_zafar = results_zafar["FNR1"]
    accuracy_zafar = results_zafar["acc"]

    results_balanced = np.load("experiments/results_balanced/disp_mis_compas_alphabeta_small.npz")
    FPR0_balanced = results_balanced["FPR0"]
    FPR1_balanced = results_balanced["FPR1"]
    FNR0_balanced = results_balanced["FNR0"]
    FNR1_balanced = results_balanced["FNR1"]
    accuracy_balanced = results_balanced["acc"]

    plt.plot(accuracy_zafar, FPR0_zafar - FPR1_zafar, 'y+', label="FPR diff Zafar et al")
    plt.plot(accuracy_zafar, FNR0_zafar - FNR1_zafar, 'gx', label="FNR diff Zafar et al")
    plt.plot(accuracy_balanced, FPR0_balanced - FPR1_balanced, 'r+', label="FPR diff current")
    plt.plot(accuracy_balanced, FNR0_balanced - FNR1_balanced, 'bx', label="FNR diff current")

    results_balanced = np.load("experiments/results_balanced/disp_mis_compas_alphabeta.npz")
    FPR0_balanced = results_balanced["FPR0"]
    FPR1_balanced = results_balanced["FPR1"]
    FNR0_balanced = results_balanced["FNR0"]
    FNR1_balanced = results_balanced["FNR1"]
    accuracy_balanced = results_balanced["acc"]
    plt.plot(accuracy_balanced, FPR0_balanced - FPR1_balanced, 'r+')
    plt.plot(accuracy_balanced, FNR0_balanced - FNR1_balanced, 'bx')

    fs = 10
    axes = plt.gca()
    axes.invert_xaxis()
    axes.set_xlabel("Accuracy")
    axes.set_ylabel("Signed error difference")
    plt.legend(fontsize=fs)
    plt.axhline(axes=axes, color='gray')

def plot_comparison_to_Zafar_alpha(k=30):

    results_zafar = np.load("experiments/results_Zafar/disp_mis_compas_cons_type_1.npz")
    FPR0_zafar = results_zafar["FPR0"]
    FPR1_zafar = results_zafar["FPR1"]
    FNR0_zafar = results_zafar["FNR0"]
    FNR1_zafar = results_zafar["FNR1"]
    accuracy_zafar = results_zafar["acc"]

    results_balanced = np.load("experiments/results_balanced/disp_mis_compas_alpha_small.npz")
    FPR0_balanced = results_balanced["FPR0"]
    FPR1_balanced = results_balanced["FPR1"]
    FNR0_balanced = results_balanced["FNR0"]
    FNR1_balanced = results_balanced["FNR1"]
    accuracy_balanced = results_balanced["acc"]

    plt.plot(accuracy_zafar, FPR0_zafar - FPR1_zafar, 'y+', label="FPR diff Zafar et al")
    plt.plot(accuracy_zafar, FNR0_zafar - FNR1_zafar, 'gx', label="FNR diff Zafar et al")
    plt.plot(accuracy_balanced, FPR0_balanced - FPR1_balanced, 'r+', label="FPR diff current")
    plt.plot(accuracy_balanced, FNR0_balanced - FNR1_balanced, 'bx', label="FNR diff current")

    results_balanced = np.load("experiments/results_balanced/disp_mis_compas_alpha.npz")
    FPR0_balanced = results_balanced["FPR0"]
    FPR1_balanced = results_balanced["FPR1"]
    FNR0_balanced = results_balanced["FNR0"]
    FNR1_balanced = results_balanced["FNR1"]
    accuracy_balanced = results_balanced["acc"]
    plt.plot(accuracy_balanced, FPR0_balanced - FPR1_balanced, 'r+')
    plt.plot(accuracy_balanced, FNR0_balanced - FNR1_balanced, 'bx')

    fs = 10
    axes = plt.gca()
    axes.invert_xaxis()
    axes.set_xlabel("Accuracy")
    axes.set_ylabel("Signed error difference")
    axes.set_ylim([-0.35, 0.35])
    plt.legend(fontsize=fs)
    plt.axhline(axes=axes, color='gray')

def plot_comparison_to_Zafar_beta(k=30):

    results_zafar = np.load("experiments/results_Zafar/disp_mis_compas_cons_type_2.npz")
    FPR0_zafar = results_zafar["FPR0"]
    FPR1_zafar = results_zafar["FPR1"]
    FNR0_zafar = results_zafar["FNR0"]
    FNR1_zafar = results_zafar["FNR1"]
    accuracy_zafar = results_zafar["acc"]

    results_balanced = np.load("experiments/results_balanced/disp_mis_compas_beta_small.npz")
    FPR0_balanced = results_balanced["FPR0"]
    FPR1_balanced = results_balanced["FPR1"]
    FNR0_balanced = results_balanced["FNR0"]
    FNR1_balanced = results_balanced["FNR1"]
    accuracy_balanced = results_balanced["acc"]

    plt.plot(accuracy_zafar, FPR0_zafar - FPR1_zafar, 'y+', label="FPR diff Zafar et al")
    plt.plot(accuracy_zafar, FNR0_zafar - FNR1_zafar, 'gx', label="FNR diff Zafar et al")
    plt.plot(accuracy_balanced, FPR0_balanced - FPR1_balanced, 'r+', label="FPR diff current")
    plt.plot(accuracy_balanced, FNR0_balanced - FNR1_balanced, 'bx', label="FNR diff current")

    results_balanced = np.load("experiments/results_balanced/disp_mis_compas_beta.npz")
    FPR0_balanced = results_balanced["FPR0"]
    FPR1_balanced = results_balanced["FPR1"]
    FNR0_balanced = results_balanced["FNR0"]
    FNR1_balanced = results_balanced["FNR1"]
    accuracy_balanced = results_balanced["acc"]
    plt.plot(accuracy_balanced, FPR0_balanced - FPR1_balanced, 'r+')
    plt.plot(accuracy_balanced, FNR0_balanced - FNR1_balanced, 'bx')

    fs = 10
    axes = plt.gca()
    axes.invert_xaxis()
    axes.set_xlabel("Accuracy")
    axes.set_ylabel("Signed error difference")
    plt.legend(fontsize=fs)
    plt.axhline(axes=axes, color='gray')

if __name__ == '__main__':
    #plot_vary_beta_experiment( 0, [2.0**i for i in range (0,15)], 'logistic', 'FPR_and_FNR_Zafar_samesign_N_10000', k=4, N=10000, log_plot=True)
    plot_compas_experiment(alphas=[2.0**i for i in range(0,11)], betas=[0], base_method='logistic', data_file_name="compas-scores-two-years-clean")
    #plot_vary_k_experiment(alpha=100, beta=100, ks=[i for i in range (1,100)], base_method='logistic', data_file_name="compas-scores-two-years-clean", N=10000)
    #plot_vary_alphabeta_experiment(alphabetas=[2.0**i for i in range(0,15)], base_method="logistic", data_file_name="FPR_and_FNR_Zafar_samesign_N_10000", k=4, N=10000, log_plot=True)
    #plot_comparison_to_Zafar_alphabeta()
    plt.show()