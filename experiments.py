import glob
import itertools
from multiprocessing import Process

import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold

import algorithm as alg
import data_handling as dh
import plots
import measuring
from random import shuffle


def run_experiment():
    # TODO: Read conf file and set up experiment with it
    # TODO: Print output and parameters to output file
    #N = 10000

    #X, t, z = dh.load_data(data_file_name)
    #vary_k_experiment(X, t, z, 1, 1, ks=range(93, 101), base_method='linear', data_file_name=data_file_name)
    #vary_k_experiment(X, t, z, 1, 1, ks=range(26, 101), base_method='logistic', data_file_name=data_file_name)
    #plot_vary_k_experiment(1, 1, base_method='linear', data_file_name=data_file_name)
    #plot_vary_k_experiment(1, 1, base_method='logistic', data_file_name=data_file_name)
    #plt.show()

    data_file_name = "compas-scores-two-years-clean"
    X, t, z = dh.load_data(data_file_name)
    #vary_k_experiment(X, t, z, 1, 1, ks=range(77, 101), base_method='linear', data_file_name=data_file_name)
    vary_k_experiment(X, t, z, 1, 1, ks=range(99, 101), base_method='logistic', data_file_name=data_file_name)

    #plot_vary_alpha_beta_experiment_with_boundaries(data_file_name, base_method='logistic', stepsize=1, n=5,m=5)

    #dh.save_data(X, t, z, "compas-scores-two-years-clean")
    #vary_alpha_beta_experiment(X, t, z, alphas=range(0, 20, 5), betas=range(0, 20, 5), k=5, base_method='linear', data_file_name="compas-scores-two-years-clean")
    #X, t, z = dh.load_data("compas-scores-two-years-clean")
    #vary_alpha_beta_experiment(X, t, z, alphas=range(0, 15, 5), betas=range(0, 20, 5), k=5, base_method='logistic',
    #                           data_file_name="compas-scores-two-years-clean")
    #plot_vary_alpha_beta_experiment_with_boundaries("compas-scores-two-years-clean", base_method='linear', stepsize=5, n=4, m=4)
    #for i in range(1, 11):
    #    N = 1000*i
    #    data_file_name = "FPR_and_FNR_Zafar_diffsign_N_" + str(N)
        #X, t, z = dh.generate_unbalanced_FPR_and_FNR_diff_sign_sample_data(N)
        #dh.save_data(X, t, z, data_file_name)
    #    X, t, z = dh.load_data(data_file_name)
    #    normalization_experiment(X, t, z, alpha=1, beta=1, k=2, base_method='linear', data_file_name=data_file_name)
    #    normalization_experiment(X, t, z, alpha=1, beta=1, k=2, base_method='logistic', data_file_name=data_file_name)

    #plot_normalization_experiment("FPR_and_FNR_Zafar_diffsign", with_base=True, base_method='linear', alpha=1, beta=1)

    #plot_normalization_experiment("FPR_and_FNR_Zafar_diffsign", with_base=False, base_method='linear', alpha=1, beta=1)

    #plot_normalization_experiment("FPR_and_FNR_Zafar_diffsign", with_base=True, base_method='logistic', alpha=1, beta=1)

    #plot_normalization_experiment("FPR_and_FNR_Zafar_diffsign", with_base=False, base_method='logistic', alpha=1, beta=1)

    #plt.show()

def fair_experiment_only_FPR():
    # Starting vary alpha beta experiment for data file FPR_Zafar_N_10000 and (alpha, beta)=(11.3, 0) with base method linear
    print ("----------------Starting fair experiment only FPR----------------------")
    k = 30
    max_val = 201
    data_file_name = "FPR_Zafar_N_10000"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(113, max_val)], betas=[0], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)

def fair_experiment_only_FNR():
    # Starting vary alpha beta experiment for data file FNR_Zafar_N_10000 and (alpha, beta)=(0, 8.8) with base method linear
    print("----------------Starting fair experiment only FNR----------------------")
    k = 30
    max_val = 201
    data_file_name = "FNR_Zafar_N_10000"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(88, max_val)], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)

def fair_experiment_diffsign():
    print("----------------Starting fair experiment diffsign----------------------")
    k = 30
    max_val = 101
    data_file_name = "FPR_and_FNR_Zafar_diffsign_N_10000"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name)

    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name)

def fair_experiment_samesign():
    # Starting vary alpha beta experiment for data file FPR_and_FNR_Zafar_samesign_N_10000 and (alpha, beta)=(13.2, 13.2) with base method linear
    print("----------------Starting fair experiment samesign----------------------")
    k = 10
    max_val = 15
    data_file_name = "FPR_and_FNR_Zafar_samesign_N_10000"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[2.0**i for i in range(0, max_val)], betas=[0], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[2.0**i for i in range(0, max_val)], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    for i in range(0, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[2.0**i], betas=[2.0**i], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name)

    vary_alpha_beta_experiment(X, t, z, alphas=[2.0**i for i in range(0, max_val)], betas=[0], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[2.0**i for i in range(0, max_val)], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    for i in range(0, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[2.0**i], betas=[2.0**i], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name)
    """vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    for i in range(132, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name)

    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name)"""

def comparison_experiment_compas():
    def split_into_train_test(x_all, y_all, x_control_all, train_fold_size):
        split_point = int(round(float(x_all.shape[0]) * train_fold_size))
        x_all_train = x_all[:split_point]
        x_all_test = x_all[split_point:]
        y_all_train = y_all[:split_point]
        y_all_test = y_all[split_point:]

        x_control_all_train = x_control_all[:split_point]
        x_control_all_test = x_control_all[split_point:]

        return x_all_train, y_all_train, x_control_all_train, x_all_test, y_all_test, x_control_all_test

    print("----------------Starting fair experiment COMPAS----------------------")
    k = 30
    max_val = 11
    FPR0_alpha = np.zeros(max_val)
    FPR1_alpha = np.zeros(max_val)
    FNR0_alpha = np.zeros(max_val)
    FNR1_alpha = np.zeros(max_val)
    accuracy_alpha = np.zeros(max_val)
    FPR0_beta = np.zeros(max_val)
    FPR1_beta = np.zeros(max_val)
    FNR0_beta = np.zeros(max_val)
    FNR1_beta = np.zeros(max_val)
    accuracy_beta = np.zeros(max_val)
    FPR0_alphabeta = np.zeros(max_val)
    FPR1_alphabeta = np.zeros(max_val)
    FNR0_alphabeta = np.zeros(max_val)
    FNR1_alphabeta = np.zeros(max_val)
    accuracy_alphabeta = np.zeros(max_val)
    data_file_name = "compas-scores-two-years-clean"

    for j in range(0, 5):
        X, y, x_control = dh.load_data(data_file_name)
        perm = [i for i in range(0, X.shape[0])]
        shuffle(perm)
        X = X[perm]
        y = y[perm]
        x_control = x_control[perm]

        train_fold_size = 0.5
        x_train, y_train, x_control_train, x_test, y_test, x_control_test = split_into_train_test(X, y, x_control,
                                                                                                     train_fold_size)
        I0 = x_control_test < 0.5
        I1 = x_control_test > 0.5
        y0_test = y_test[I0]
        y1_test = y_test[I1]
        x0_test = x_test[I0, :]
        x1_test = x_test[I1, :]

        for factor in range(0, max_val):
            logreg_alpha = alg.BalancedLogisticRegressionClassifier(x_train, y_train, x_control_train, alpha=factor/10,
                                                                    beta=0, k=k)
            logreg_alpha.fit()
            logreg_beta = alg.BalancedLogisticRegressionClassifier(x_train, y_train, x_control_train, alpha=0,
                                                                    beta=factor/10, k=k)
            logreg_beta.fit()
            logreg_alphabeta = alg.BalancedLogisticRegressionClassifier(x_train, y_train, x_control_train, alpha=factor/10,
                                                                    beta=factor/10, k=k)
            logreg_alphabeta.fit()

            w = logreg_alpha.w
            FNR0, FPR0 = measuring.get_FNR_and_FPR(x0_test, y0_test, w)
            FNR1, FPR1 = measuring.get_FNR_and_FPR(x1_test, y1_test, w)
            accuracy = measuring.get_accuracy(x_test, y_test, w)

            FPR0_alpha[factor] += FPR0 / 5
            FPR1_alpha[factor] += FPR1 / 5
            FNR0_alpha[factor] += FNR0 / 5
            FNR1_alpha[factor] += FNR1 / 5
            accuracy_alpha[factor] += accuracy / 5

            w = logreg_beta.w
            FNR0, FPR0 = measuring.get_FNR_and_FPR(x0_test, y0_test, w)
            FNR1, FPR1 = measuring.get_FNR_and_FPR(x1_test, y1_test, w)
            accuracy = measuring.get_accuracy(x_test, y_test, w)

            FPR0_beta[factor] += FPR0 / 5
            FPR1_beta[factor] += FPR1 / 5
            FNR0_beta[factor] += FNR0 / 5
            FNR1_beta[factor] += FNR1 / 5
            accuracy_beta[factor] += accuracy / 5

            w = logreg_alphabeta.w
            FNR0, FPR0 = measuring.get_FNR_and_FPR(x0_test, y0_test, w)
            FNR1, FPR1 = measuring.get_FNR_and_FPR(x1_test, y1_test, w)
            accuracy = measuring.get_accuracy(x_test, y_test, w)

            FPR0_alphabeta[factor] += FPR0 / 5
            FPR1_alphabeta[factor] += FPR1 / 5
            FNR0_alphabeta[factor] += FNR0 / 5
            FNR1_alphabeta[factor] += FNR1 / 5
            accuracy_alphabeta[factor] += accuracy / 5



    filename = "experiments/results_balanced/disp_mis_compas_alpha_small"
    np.savez(filename, FPR0=FPR0_alpha, FPR1=FPR1_alpha, FNR0=FNR0_alpha, FNR1=FNR1_alpha, acc=accuracy_alpha)
    filename = "experiments/results_balanced/disp_mis_compas_beta_small"
    np.savez(filename, FPR0=FPR0_beta, FPR1=FPR1_beta, FNR0=FNR0_beta, FNR1=FNR1_beta, acc=accuracy_beta)
    filename = "experiments/results_balanced/disp_mis_compas_alphabeta_small"
    np.savez(filename, FPR0=FPR0_alphabeta, FPR1=FPR1_alphabeta, FNR0=FNR0_alphabeta, FNR1=FNR1_alphabeta, acc=accuracy_alphabeta)

def fair_experiment_compas():
    # Starting vary alpha beta experiment for data file compas-scores-two-years-clean_fold_3 and (alpha, beta)=(7.6, 7.6) with base method logistic
    print("----------------Starting fair experiment COMPAS----------------------")
    k = 30
    max_val = 11
    data_file_name = "compas-scores-two-years-clean"
    five_fold_indices = np.load("data/" + data_file_name + "_5_fold_indices.npz")
    X, t, z = dh.load_data(data_file_name)

    for j in range(0, 5):
        X_train = X[five_fold_indices['train_indices'][j]]
        t_train = t[five_fold_indices['train_indices'][j]]
        z_train = z[five_fold_indices['train_indices'][j]]
        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[2.0**i for i in range(0, max_val)], betas=[0],
                                   k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[0], betas=[2.0**i for i in range(0, max_val)],
                                   k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        for i in range(0, max_val):
            vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[2.0**i], betas=[2.0**i], k=k,
                                       base_method='linear',
                                       data_file_name=data_file_name + "_fold_" + str(j))

        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[2.0**i for i in range(0, max_val)], betas=[0],
                                   k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[0], betas=[2.0**i for i in range(0, max_val)],
                                   k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        for i in range(0, max_val):
            vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[2.0**i], betas=[2.0**i], k=k,
                                       base_method='logistic',
                                       data_file_name=data_file_name + "_fold_" + str(j))

        """vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        for i in range(1, max_val):
            vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[i / 10], betas=[i / 10], k=k,
                                       base_method='linear',
                                       data_file_name=data_file_name + "_fold_" + str(j))

        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name + "_fold_" + str(j))
        for i in range(1, max_val):
            vary_alpha_beta_experiment(X_train, t_train, z_train, alphas=[i / 10], betas=[i / 10], k=k,
                                       base_method='logistic',
                                       data_file_name=data_file_name + "_fold_" + str(j))"""


def sequential_fair_experiments():
    k = 30
    max_val = 201
    data_file_name = "FPR_and_FNR_Zafar_diffsign_N_10000"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k, base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k, base_method='linear',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name)

    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k, base_method='logistic',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k, base_method='logistic',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name)

    data_file_name = "FPR_and_FNR_Zafar_samesign_N_10000"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name)

    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name)

    data_file_name = "compas-scores-two-years-clean"
    X, t, z = dh.load_data(data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='linear',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='linear',
                                   data_file_name=data_file_name)

    vary_alpha_beta_experiment(X, t, z, alphas=[i / 10 for i in range(0, max_val)], betas=[0], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    vary_alpha_beta_experiment(X, t, z, alphas=[0], betas=[i / 10 for i in range(0, max_val)], k=k,
                               base_method='logistic',
                               data_file_name=data_file_name)
    for i in range(1, max_val):
        vary_alpha_beta_experiment(X, t, z, alphas=[i / 10], betas=[i / 10], k=k,
                                   base_method='logistic',
                                   data_file_name=data_file_name)



def vary_k_experiment(X, t, z, alpha, beta, ks, base_method, data_file_name):
    for k in ks:
        print("Started vary_alpha_beta_experiment with k=" +str(k))
        vary_alpha_beta_experiment(X, t, z, alphas=[alpha], betas=[beta], k=k, base_method=base_method, data_file_name=data_file_name)




def normalization_experiment(X, t, z, base_method, data_file_name, alpha=10, beta=10, k=10, scaling="none", train_size=1.0):
    model = 0
    X_train = X
    t_train = t
    z_train = z
    if train_size < 1.0:
        X_train, X_test, t_train, t_test, z_train, z_test = train_test_split(X, t, z, train_size=train_size, random_state=77)

    if (base_method == 'linear'):
        model = alg.BalancedLinearRegressionClassifier(X_train, t_train, z_train, alpha=alpha, beta=beta, k=k, scaling=scaling)

    elif (base_method == 'logistic'):
        model = alg.BalancedLogisticRegressionClassifier(X_train, t_train, z_train, alpha=alpha, beta=beta, k=k, scaling=scaling)

    #model.initial_solution()
    mu = 1
    tau = 0.6
    c = 0.5
    i = 0

    base_method_errors = [model.base_method_error(model.w)]
    squared_FNR_diff_errors = [model.squared_FNR_difference_error(model.w)]
    squared_FPR_diff_errors = [model.squared_FPR_difference_error(model.w)]
    old_cost = model.cost_function(model.w)
    model.gradient_descent_iteration(mu, c, tau)
    new_cost = model.cost_function(model.w)
    #print("Old cost: " + str(old_cost) + ", and new cost: " + str(new_cost))
    i += 1
    while (abs(old_cost - new_cost)/old_cost > 0.00001 and i < 200):
        old_cost = new_cost
        base_method_errors.append(model.base_method_error(model.w))
        squared_FNR_diff_errors.append(model.squared_FNR_difference_error(model.w))
        squared_FPR_diff_errors.append(model.squared_FPR_difference_error(model.w))
        model.gradient_descent_iteration(mu, c, tau)
        new_cost = model.cost_function(model.w)

        i += 1
    print("Gradient descent for normalization experiment terminated after " + str(i) + " iterations.\n")


    np_base_errors = np.atleast_1d(base_method_errors)
    np_FNR_diff = np.atleast_1d(squared_FNR_diff_errors)
    np_FPR_diff = np.atleast_1d(squared_FPR_diff_errors)

    np.savez("experiments/normalization_with_base_" + data_file_name + "_base_method_" + str(base_method) + "_alpha_" + str(
        alpha) + "_beta_" + str(beta) + "_scaling_" + scaling + "_train_size_" + str(train_size), base_errors=np_base_errors, FNR_diff=np_FNR_diff, FPR_diff=np_FPR_diff, scaling=scaling, train_size=train_size)


def convergence_experiment(X, t, z, alpha, beta, k, base_method, data_file_name):
    model = 0
    if (base_method == 'linear'):
        model = alg.BalancedLinearRegressionClassifier(X, t, z, alpha=alpha, beta=beta, k=k)

    elif (base_method == 'logistic'):
        model = alg.BalancedLogisticRegressionClassifier(X, t, z, alpha=alpha, beta=beta, k=k)

    #model.initial_solution()
    errors = [model.cost_function(model.w)]
    for i in range(0, 100):
        if (i % 10 == 0):
            print("Reached iteration number " + str(i) + " for convergence experiment.")
        model.gradient_descent_iteration(1, 0.5, 0.5)
        errors.append(model.cost_function(model.w))

    def pairwise(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = itertools.tee(iterable)
        next(b, None)
        return zip(a, b)

    np_errors = np.atleast_1d(errors)
    np_improvements = np.fromiter((x - y for (x, y) in pairwise(errors)), np.float)

    np.savez("experiments/convergence_" + data_file_name + "_base_method_" + str(base_method) + "_alpha_" + str(alpha) + "_beta_" + str(beta), errors=np_errors, improvements=np_improvements)

    pass




def vary_alpha_beta_experiment(X, t, z, alphas, betas, k, base_method, data_file_name):
    for alpha in alphas:
        for beta in betas:
            print("Starting vary alpha beta experiment for data file " + data_file_name + " and (alpha, beta)=" + str((alpha, beta)) + " with base method " + base_method)
            if (base_method == 'linear'):
                linreg = alg.BalancedLinearRegressionClassifier(X, t, z, alpha=alpha, beta=beta, k=k)
                linreg.fit()
                linreg.save_model(
                    data_file_name + "_base_method_linear_alpha_" + str(alpha) + "_beta_" + str(beta) + "_k_" + str(k) + "_N_" + str(X.shape[0]),
                    data_file_name)
            elif (base_method == 'logistic'):
                logreg = alg.BalancedLogisticRegressionClassifier(X, t, z, alpha=alpha, beta=beta, k=k)
                logreg.fit()
                logreg.save_model(
                    data_file_name + "_base_method_logistic_alpha_" + str(alpha) + "_beta_" + str(beta) + "_k_" + str(k) + "_N_" + str(X.shape[0]),
                    data_file_name)

def vary_k_experiments():
    """data_file_name = "FPR_and_FNR_Zafar_diffsign_N_10000"
        X, t, z = dh.load_data(data_file_name)
        for i in range(0, 3):
            print("\nIteration " + str(i) + " of the experiment on dataset " + data_file_name)
            alpha = 10**i
            beta = alpha
            vary_k_experiment(X, t, z, alpha=alpha, beta=beta, ks=[i for i in range(1,101)], base_method='linear',
                              data_file_name=data_file_name)
            vary_k_experiment(X, t, z, alpha=alpha, beta=beta, ks=[i for i in range(1, 101)], base_method='logistic',
                              data_file_name=data_file_name)"""

    data_file_name = "FPR_and_FNR_Zafar_samesign_N_10000"
    X, t, z = dh.load_data(data_file_name)
    for i in range(2, 3):
        print("\nIteration " + str(i) + " of the experiment on dataset " + data_file_name)
        alpha = 10 ** i
        beta = alpha
        # vary_k_experiment(X, t, z, alpha=alpha, beta=beta, ks=[i for i in range(63, 101)], base_method='linear',
        #                  data_file_name=data_file_name)
        vary_k_experiment(X, t, z, alpha=alpha, beta=beta, ks=[i for i in range(1, 101)], base_method='logistic',
                          data_file_name=data_file_name)

    data_file_name = "compas-scores-two-years-clean"
    X, t, z = dh.load_data(data_file_name)
    for i in range(0, 3):
        print("\nIteration " + str(i) + " of the experiment on dataset " + data_file_name)
        alpha = 10 ** i
        beta = alpha
        vary_k_experiment(X, t, z, alpha=alpha, beta=beta, ks=[i for i in range(1, 101)], base_method='linear',
                          data_file_name=data_file_name)
        vary_k_experiment(X, t, z, alpha=alpha, beta=beta, ks=[i for i in range(1, 101)], base_method='logistic',
                          data_file_name=data_file_name)

def generate_5_fold_indices(data_file_name):
    X, t, z = dh.load_data(data_file_name)
    kf = KFold(n_splits=5, random_state=None, shuffle=True)
    train_indices = []
    test_indices = []
    for train_index, test_index in kf.split(X):
        print(str(train_index))
        print(str(test_index))
        train_indices.append(train_index)
        test_indices.append(test_index)
    np.savez("data/" + data_file_name + "_5_fold_indices", train_indices=train_indices, test_indices=test_indices)

if __name__ == '__main__':
    #generate_5_fold_indices("compas-scores-two-years-clean")
    #fair_experiment_compas()
    #fair_experiment_samesign()
    comparison_experiment_compas()
    #data_file_name = "FNR_Zafar_N_10000"
    #plots.plot_boundaries_beta(data_file_name, 'linear', 0, [0.0, 1.0, 5.0, 20.0], 30)
    #plots.plot_compas_experiments()
    #plt.show()
    pass