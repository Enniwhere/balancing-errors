import numpy as np
import pandas as pd
from collections import defaultdict
from sklearn import preprocessing
from random import seed, shuffle
import csv

data_directory = "data/"

def save_data(X, t, z, filename):
    #with open(filename, "w+") as file_:
    np.savez(data_directory + filename, X=X, t=t, z=z)

def load_data(filename):
    #with open(filename) as file_:
    data = np.load(data_directory + filename + ".npz")
    return data['X'], data['t'], data['z']

def generate_unbalanced_FNR_sample_data(N=10000):
    neg_mean_0 = [2, 2]
    neg_mean_1 = [2, 2]
    pos_mean_0 = [1, 0]
    pos_mean_1 = [-2, -2]

    neg_cov_0 = [[3, 1], [1, 3]]
    neg_cov_1 = [[3, 1], [1, 3]]
    pos_cov_0 = [[3, 3], [1, 3]]
    pos_cov_1 = [[3, 1], [1, 3]]

    return generate_unbalanced_data(pos_mean_0, pos_mean_1, neg_mean_0, neg_mean_1, pos_cov_0, pos_cov_1, neg_cov_0, neg_cov_1, N)

def generate_unbalanced_FPR_sample_data(N=10000):
    neg_mean_0 = [2, 2]
    neg_mean_1 = [-1, 0]
    pos_mean_0 = [-2, -2]
    pos_mean_1 = [-2, -2]

    neg_cov_0 = [[3, 1], [1, 3]]
    neg_cov_1 = [[3, 3], [1, 3]]
    pos_cov_0 = [[3, 1], [1, 3]]
    pos_cov_1 = [[3, 1], [1, 3]]

    return generate_unbalanced_data(pos_mean_0, pos_mean_1, neg_mean_0, neg_mean_1, pos_cov_0, pos_cov_1, neg_cov_0, neg_cov_1, N)


def generate_unbalanced_FPR_and_FNR_diff_sign_sample_data(N=10000):
    neg_mean_0 = [-1, -3]
    neg_mean_1 = [-1, 0]
    pos_mean_0 = [2, 0]
    pos_mean_1 = [2, 3]

    neg_cov_0 = [[5, 1], [1, 5]]
    neg_cov_1 = [[5, 1], [1, 5]]
    pos_cov_0 = [[5, 1], [1, 5]]
    pos_cov_1 = [[5, 1], [1, 5]]

    return generate_unbalanced_data(pos_mean_0, pos_mean_1, neg_mean_0, neg_mean_1, pos_cov_0, pos_cov_1, neg_cov_0,
                                    neg_cov_1, N)

def generate_unbalanced_FPR_and_FNR_same_sign_sample_data(N=10000):

    neg_mean_0 = [0, -1]
    neg_mean_1 = [-5, 0]
    pos_mean_0 = [1, 2]
    pos_mean_1 = [2, 3]

    neg_cov_0 = [[7, 1], [1, 7]]
    neg_cov_1 = [[5, 1], [1, 5]]
    pos_cov_0 = [[5, 2], [2, 5]]
    pos_cov_1 = [[10, 1], [1, 4]]

    return generate_unbalanced_data(pos_mean_0, pos_mean_1, neg_mean_0, neg_mean_1, pos_cov_0, pos_cov_1, neg_cov_0,
                                    neg_cov_1, N)


def generate_unbalanced_data(pos_mean_0, pos_mean_1, neg_mean_0, neg_mean_1, pos_cov_0, pos_cov_1, neg_cov_0, neg_cov_1, N=10000):

    N = int(N/4)

    neg0 = np.random.multivariate_normal(neg_mean_0, neg_cov_0, N)
    neg1 = np.random.multivariate_normal(neg_mean_1, neg_cov_1, N)
    pos0 = np.random.multivariate_normal(pos_mean_0, pos_cov_0, N)
    pos1 = np.random.multivariate_normal(pos_mean_1, pos_cov_1, N)

    neg_train = np.concatenate((neg0, neg1))
    pos_train = np.concatenate((pos0, pos1))
    # print ("Shape of neg_train: " + str(neg_train.shape))
    # print ("Shape of pos_train: " + str(pos_train.shape))
    X_train = np.concatenate((neg_train, pos_train))
    y_train = np.concatenate(([-1 for i in range(0, 2 * N)], [1 for i in range(0, 2 * N)]), axis=0)
    z_train = np.concatenate(([0 for i in range(0, N)], [1 for i in range(0, N)], [0 for i in range(0, N)], [1 for i in range(0, N)]), axis=0)

    return X_train, y_train, z_train

def read_compas():
    with open(data_directory + "compas-scores-two-years.csv") as csvfile:
        reader = csv.DictReader(csvfile)
        arr = []
        for row in reader:
            arr.append(row)
        arr = [row for row in arr if row['days_b_screening_arrest'] != '']
        #print("First filter done")
        arr_filtered = [row for row in arr if int(row['days_b_screening_arrest']) <= 30 and int(row['days_b_screening_arrest']) >= -30 and row['is_recid'] != -1 and row['c_charge_degree'] != "O" and row['score_text'] != 'n/A' and (row['race'] == "African-American" or row['race'] == "Caucasian")]
        #X = np.atleast_2d([[row['c_charge_degree'], row['age_cat'], row['sex'], row['priors_count']] for row in arr])
        #t = np.atleast_1d([row['two_year_recid'] for row in arr])
        #z = np.atleast_1d([row['race'] for row in arr])
        #print("Second filter done")
        #filt = [int(row['days_b_screening_arrest']) <= 30 and int(row['days_b_screening_arrest']) >= -30 and row['is_recid'] != -1 and row['c_charge_degree'] != "O" and row['score_text'] != 'n/A' and (row['race'] == "African-American" or row['race'] == "Caucasian") for row in arr]

        #X_filtered = X[filt, :]
        #t_filtered = t[filt]
        #z_filtered = z[filt]

        def race_to_num(x):
            if x == "Caucasian":
                return 1
            elif x == "African-American":
                return 0
            else:
                print("There was an unrecognized race: " + str(x))
                return -1

        def recid_to_target(x):
            if x == '1':
                return 1
            elif x == '0':
                return -1
            else:
                print("There was some unrecognized input to recid_to_target: " + str(x))

        #charge_degree_set = set(X_filtered[:, 0])
        #age_cat_set = set(X_filtered[:, 1])
        #sex_set = set(X_filtered[:, 2])
        #priors_count_set = set(X_filtered[:, 3])

        #print(str(charge_degree_set))
        #print(str(age_cat_set))
        #print(str(sex_set))
        #print(str(priors_count_set))

        def charge_degree_to_number(x):
            if x == 'F':
                return 0
            elif x == 'M':
                return 1
            else:
                print ("Unrecognized input in charge degree: " + str(x))
                return -1

        def age_cat_to_number(x):
            if x == 'Less than 25':
                return 0
            elif x == '25 - 45':
                return 1
            elif x == 'Greater than 45':
                return 2
            else:
                print ("Unrecognized input in age cat: " + str(x))
                return -1

        def gender_to_number(x):
            if x == 'Female':
                return 0
            elif x == 'Male':
                return 1
            else:
                print ("Unrecognized input in sex: " + str(x))
                return -1

        def priors_to_number(x):
            return int(x)

        def raw_to_X_data(row):
            x_0 = -1
            x_1 = -1
            x_2 = -1
            x_3 = -1
            if row[0] == 'F':
                x_0 = 0
            elif row[0] == 'M':
                x_0 = 1
            else:
                print ("Unrecognized input in column 0: " + str(row[0]))

            if row[1] == 'Less than 25':
                x_1 = 0
            elif row[1] == '25 - 45':
                x_1 = 1
            elif row[1] == 'Greater than 45':
                x_1 = 2
            else:
                print ("Unrecognized input in column 1: " + str(row[1]))

            if row[2] == 'Female':
                x_2 = 0
            elif row[2] == 'Male':
                x_2 = 1
            else:
                print ("Unrecognized input in column 2: " + str(row[2]))

            x_3 = int(row[3])

            return [x_0, x_1, x_2, x_3]

        #print("Starting the mapping process")
        X_mapped = np.atleast_2d([[charge_degree_to_number(row['c_charge_degree']), age_cat_to_number(row['age_cat']), gender_to_number(row['sex']), priors_to_number(row['priors_count']), race_to_num(row['race'])] for row in arr_filtered])
        #print("X mapped")
        t_mapped = np.atleast_1d([recid_to_target(row['two_year_recid']) for row in arr_filtered])
        #print("t mapped")
        z_mapped = np.atleast_1d([race_to_num(row['race']) for row in arr_filtered])


        return X_mapped, t_mapped, z_mapped


def load_compas_data():
    """ This code is taken almost directly from Muhammad Bilal Zafars implementation at https://github.com/mbilalzafar/fair-classification.
        The only things that have been changed is what needed to be changed in order to make the algorithm run python 3 instead of python 2"""
    FEATURES_CLASSIFICATION = ["age_cat", "race", "sex", "priors_count",
                               "c_charge_degree"]  # features to be used for classification
    CONT_VARIABLES = [
        "priors_count"]  # continuous features, will need to be handled separately from categorical features, categorical features will be encoded using one-hot
    CLASS_FEATURE = "two_year_recid"  # the decision variable
    SENSITIVE_ATTRS = ["race"]

    COMPAS_INPUT_FILE = data_directory + "compas-scores-two-years.csv"
    # check_data_file(COMPAS_INPUT_FILE)

    # load the data and get some stats
    df = pd.read_csv(COMPAS_INPUT_FILE)

    # convert to np array
    data = df.to_dict('list')
    for k in data.keys():
        data[k] = np.array(data[k])

    """ Filtering the data """

    # These filters are the same as propublica (refer to https://github.com/propublica/compas-analysis)
    # If the charge date of a defendants Compas scored crime was not within 30 days from when the person was arrested, we assume that because of data quality reasons, that we do not have the right offense.
    idx = np.logical_and(data["days_b_screening_arrest"] <= 30, data["days_b_screening_arrest"] >= -30)

    # We coded the recidivist flag -- is_recid -- to be -1 if we could not find a compas case at all.
    idx = np.logical_and(idx, data["is_recid"] != -1)

    # In a similar vein, ordinary traffic offenses -- those with a c_charge_degree of 'O' -- will not result in Jail time are removed (only two of them).
    idx = np.logical_and(idx, data["c_charge_degree"] != "O")  # F: felony, M: misconduct

    # We filtered the underlying data from Broward county to include only those rows representing people who had either recidivated in two years, or had at least two years outside of a correctional facility.
    idx = np.logical_and(idx, data["score_text"] != "NA")

    # we will only consider blacks and whites for this analysis
    idx = np.logical_and(idx, np.logical_or(data["race"] == "African-American", data["race"] == "Caucasian"))

    # select the examples that satisfy this criteria
    for k in data.keys():
        data[k] = data[k][idx]

    """ Feature normalization and one hot encoding """

    # convert class label 0 to -1
    y = data[CLASS_FEATURE]
    y[y == 0] = -1

    print("\nNumber of people recidivating within two years")
    print(pd.Series(y).value_counts())
    print()
    "\n"

    X = np.array([]).reshape(len(y),
                             0)  # empty array with num rows same as num examples, will hstack the features to it
    x_control = defaultdict(list)

    feature_names = []
    for attr in FEATURES_CLASSIFICATION:
        vals = data[attr]
        if attr in CONT_VARIABLES:
            vals = [float(v) for v in vals]
            vals = preprocessing.scale(vals)  # 0 mean and 1 variance
            vals = np.reshape(vals, (len(y), -1))  # convert from 1-d arr to a 2-d arr with one col

        else:  # for binary categorical variables, the label binarizer uses just one var instead of two
            lb = preprocessing.LabelBinarizer()
            lb.fit(vals)
            vals = lb.transform(vals)

        # add to sensitive features dict
        if attr in SENSITIVE_ATTRS:
            x_control[attr] = vals

        # add to learnable features
        X = np.hstack((X, vals))

        if attr in CONT_VARIABLES:  # continuous feature, just append the name
            feature_names.append(attr)
        else:  # categorical features
            if vals.shape[1] == 1:  # binary features that passed through lib binarizer
                feature_names.append(attr)
            else:
                for k in lb.classes_:  # non-binary categorical features, need to add the names for each cat
                    feature_names.append(attr + "_" + str(k))

    # convert the sensitive feature to 1-d array
    x_control = dict(x_control)
    for k in x_control.keys():
        assert (x_control[k].shape[1] == 1)  # make sure that the sensitive feature is binary after one hot encoding
        x_control[k] = np.array(x_control[k]).flatten()

    # sys.exit(1)

    """permute the date randomly"""
    perm = [i for i in range(0, X.shape[0])]
    shuffle(perm)
    X = X[perm]
    y = y[perm]
    for k in x_control.keys():
        x_control[k] = x_control[k][perm]

    def add_intercept(x):

        """ Add intercept to the data before linear classification """
        m, n = x.shape
        intercept = np.ones(m).reshape(m, 1)  # the constant b
        return np.concatenate((intercept, x), axis=1)

    X = add_intercept(X)

    feature_names = ["intercept"] + feature_names
    assert (len(feature_names) == X.shape[1])
    print("Features we will be using for classification are:", feature_names, "\n")

    return X, y, x_control
