import numpy as np
import sklearn as sk
from sklearn import preprocessing
import math
import abc
import os
import measuring
from sklearn.linear_model import LinearRegression, LogisticRegression


class BalancedClassifier(abc.ABC):

    MAX_ITERATIONS = 200

    def get_model_directory(self):
        return "models/"

    def __init__(self, X, t, z, alpha=0, beta=0, k=1, scaling="none"):
        self.t = t
        self.z = z
        self.alpha = alpha
        self.beta = beta
        self.k = k
        self.scaling = scaling

        self.n = t.shape[0]
        self.d = X.shape[1] + 1
        self.w = np.zeros(self.d)
        if (self.scaling == "standardize"):
            self.scaler = preprocessing.StandardScaler()
            X = self.scaler.fit_transform(X)

        self.X = np.concatenate((np.ones((self.n, 1)), X), axis=1)



        assert self.X.shape == (self.n, self.d)

        self.n1 = np.sum(self.z)
        self.n0 = self.n - self.n1

        I0 = self.z < 0.5
        I1 = self.z > 0.5
        self.X0 = self.X[I0, :]
        self.X1 = self.X[I1, :]
        self.t0 = self.t[I0]
        self.t1 = self.t[I1]

        self.N0 = sum([1 for t in self.t0 if t > 0])
        self.N1 = sum([1 for t in self.t1 if t > 0])
        self.P0 = sum([1 for t in self.t0 if t < 0])
        self.P1 = sum([1 for t in self.t1 if t > 0])

    @abc.abstractmethod
    def base_method_solution(self):
        return

    @abc.abstractmethod
    def base_method_gradient(self, w):
        return

    @abc.abstractmethod
    def base_method_error(self, w):
        return

    @abc.abstractmethod
    def base_method_normalization(self):
        return

    @abc.abstractmethod
    def save_model(self, filename, data_file_name):
        return

    def fit(self, mu=1, c=0.5, tau=0.5, convergence_criterion='rel_tol', convergence_var=0.00001, use_heuristic=True):

        if use_heuristic:
            self.initial_solution()
        else:
            self.w_base = np.zeros(self.d)
            self.w = np.zeros(self.d)
        i = 0
        if (convergence_criterion == 'iterations'):
            for i in range(0, convergence_var):

                self.gradient_descent_iteration(mu, c, tau)
                #print("w after update: " + str(w))
        elif (convergence_criterion == 'rel_tol'):
            old_cost = self.cost_function(self.w)
            self.gradient_descent_iteration(mu, c, tau)

            i+=1
            while abs(old_cost - self.cost_function(self.w))/old_cost > convergence_var and i < self.MAX_ITERATIONS:
                old_cost = self.cost_function(self.w)
                self.gradient_descent_iteration(mu, c, tau)

                i+=1
            print ("Gradient descent terminated after " + str(i) + " iterations.\n")

        return self

    def initial_solution(self):
        self.w_base = self.base_method_solution()
        self.w = self.w_base

    def gradient_descent_iteration(self, mu, c, tau):
        BM_grad = self.base_method_gradient(self.w) / self.base_method_normalization()
        FPR_grad = self.FPR_difference_gradient(self.w)
        FNR_grad = self.FNR_difference_gradient(self.w)
        L_mark_tilde_grad = (BM_grad + FPR_grad * self.alpha + FNR_grad * self.beta)
        #print("BM gradient found: " + str(BM_grad))
        #print("Total grad found: " + str(L_mark_tilde_grad))
        stepsize = self.backtracking_line_search(L_mark_tilde_grad, mu, c, tau)
        #print("Got a step size of " + str(stepsize) + " from line search")
        self.w = self.w - L_mark_tilde_grad * stepsize

    def backtracking_line_search(self, L_mark_tilde_grad, mu, c, tau):
        L_mark_tilde_grad_norm_squared = np.linalg.norm(L_mark_tilde_grad) ** 2

        # Do beam search to find mu

        L_mark_tilde = self.cost_function(self.w)

        w_stepped = self.w - mu * L_mark_tilde_grad
        L_mark_tilde_stepped = self.cost_function(w_stepped)


        while L_mark_tilde_stepped > L_mark_tilde - mu * c * L_mark_tilde_grad_norm_squared:
            #print("Stepped cost: " + str(L_mark_tilde_stepped) + ", and previous cost: " + str(L_mark_tilde))
            mu *= tau
            w_stepped = self.w - mu * L_mark_tilde_grad
            L_mark_tilde_stepped = self.cost_function(w_stepped)


        return mu


    def FPR_difference_gradient(self, w):

        y0 = np.fromiter((sigmoid(np.dot(self.X0[i, :].T, w.T), self.k) for i in range(0, self.n0)), np.float, count=self.n0)
        y1 = np.fromiter((sigmoid(np.dot(self.X1[i, :].T, w.T), self.k) for i in range(0, self.n1)), np.float, count=self.n1)
        #print(str(range(0, self.n0)))
        #y0 = np.fromiter((sigmoid(np.dot(self.X0[i, :].T, w.T), self.k) for i in range(0, self.n0)), np.float)
        #y1 = np.fromiter((sigmoid(np.dot(self.X1[i, :].T, w.T), self.k) for i in range(0, self.n1)), np.float)

        assert y0.shape[0] == self.n0
        assert y1.shape[0] == self.n1

        R0 = np.zeros((self.n0, self.n0))
        for i in range(0, self.n0):
            R0[i, i] = y0[i] * (1 - y0[i])

        R1 = np.zeros((self.n1, self.n1))
        for i in range(0, self.n1):
            R1[i, i] = y1[i] * (1 - y1[i])

        Ht0neg = np.fromiter((heaviside(-self.t0[i]) for i in range(0, self.n0)), np.float, count=self.n0)
        Ht1neg = np.fromiter((heaviside(-self.t1[i]) for i in range(0, self.n1)), np.float, count=self.n1)

        assert Ht0neg.shape[0] == self.n0
        assert Ht1neg.shape[0] == self.n1

        FP0_tilde = np.dot(y0, Ht0neg.T)
        FP1_tilde = np.dot(y1, Ht1neg.T)

        dFP0_tilde = np.dot(self.X0.T, np.dot(R0, Ht0neg.T))
        dFP1_tilde = np.dot(self.X1.T, np.dot(R1, Ht1neg.T))

        assert dFP0_tilde.shape[0] == self.d
        assert dFP1_tilde.shape[0] == self.d


        grad = 2 * self.k * (1 / (self.N0 * self.N0) * FP0_tilde * dFP0_tilde +
                             1 / (self.N1 * self.N1) * FP1_tilde * dFP1_tilde -
                             1 / (self.N0 * self.N1) * (FP0_tilde * dFP1_tilde + FP1_tilde * dFP0_tilde))

        return grad

    def FNR_difference_gradient(self, w):

        y0 = np.fromiter((sigmoid(np.dot(self.X0[i, :].T, w), self.k) for i in range(0, self.n0)), np.float, count=self.n0)
        y1 = np.fromiter((sigmoid(np.dot(self.X1[i, :].T, w), self.k) for i in range(0, self.n1)), np.float, count=self.n1)

        assert y0.shape[0] == self.n0
        assert y1.shape[0] == self.n1

        R0 = np.zeros((self.n0, self.n0))
        for i in range(0, self.n0):
            R0[i, i] = y0[i] * (1 - y0[i])

        R1 = np.zeros((self.n1, self.n1))
        for i in range(0, self.n1):
            R1[i, i] = y1[i] * (1 - y1[i])

        Ht0 = np.fromiter((heaviside(self.t0[i]) for i in range(0, self.n0)), np.float, count=self.n0)
        Ht1 = np.fromiter((heaviside(self.t1[i]) for i in range(0, self.n1)), np.float, count=self.n1)

        assert Ht0.shape[0] == self.n0
        assert Ht1.shape[0] == self.n1

        FP0_tilde = np.dot(np.ones(self.n0) - y0, Ht0.T)
        FP1_tilde = np.dot(np.ones(self.n1) - y1, Ht1.T)

        dFP0_tilde = np.dot(self.X0.T, np.dot(R0, Ht0.T))
        dFP1_tilde = np.dot(self.X1.T, np.dot(R1, Ht1.T))

        assert dFP0_tilde.shape[0] == self.d
        assert dFP1_tilde.shape[0] == self.d

        grad = 2 * self.k * ((-1) / (self.P0 * self.P0) * FP0_tilde * dFP0_tilde
                             - 1 / (self.P1 * self.P1) * FP1_tilde * dFP1_tilde
                             + 1 / (self.P0 * self.P1) * (FP0_tilde * dFP1_tilde + FP1_tilde * dFP0_tilde))

        return grad


    def cost_function(self, w):
        return self.base_method_error(w) / self.base_method_normalization() \
                       + self.alpha * self.squared_FPR_difference_error(w) \
                       + self.beta * self.squared_FNR_difference_error(w)

    def squared_FPR_difference_error(self, w):
        FPR0 = 0
        FPR1 = 1

        for i in range(0, self.n0):
            FPR0 += heaviside(-self.t0[i]) * sigmoid(np.dot(self.X0[i, :], w.T), self.k)
        FPR0 = FPR0 / self.N0
        for i in range(0, self.n1):
            FPR1 += heaviside(-self.t1[i]) * sigmoid(np.dot(self.X1[i, :], w.T), self.k)
        FPR1 = FPR1 / self.N1

        return (FPR0 - FPR1)**2

    def squared_FNR_difference_error(self, w):
        FNR0 = 0
        FNR1 = 1

        for i in range(0, self.n0):
            FNR0 += heaviside(self.t0[i]) * sigmoid(-np.dot(self.X0[i, :], w.T), self.k)
        FNR0 = FNR0 / self.P0
        for i in range(0, self.n1):
            FNR1 += heaviside(self.t1[i]) * sigmoid(-np.dot(self.X1[i, :], w.T), self.k)
        FNR1 = FNR1 / self.P1

        return (FNR0 - FNR1)**2

    def get_predictive_statistics(self, X, t, z):
        if self.scaling == "standardize":
            X_scaled = self.scaler.transform(X)
            return measuring.get_predictive_statistics_decision_boundary(X_scaled, t, z)
        return measuring.get_predictive_statistics_decision_boundary(X, t, z)

    def get_FNR_and_FPR(self, X, t, w):
        if self.scaling == "standardize":
            X_scaled = self.scaler.transform(X)
            return measuring.get_FNR_and_FPR(X_scaled, t, w)
        return measuring.get_FNR_and_FPR(X, t, w)

    def get_accuracy(self, X, t, w):
        if self.scaling == "standardize":
            X_scaled = self.scaler.transform(X)
            return measuring.get_accuracy(X_scaled, t, w)
        return measuring.get_accuracy(X, t, w)



class BalancedLinearRegressionClassifier(BalancedClassifier):

    def base_method_solution(self):
        linreg = LinearRegression().fit(self.X[:,1:], self.t)
        return np.reshape(np.append(linreg.intercept_, linreg.coef_), (self.d))

    def base_method_gradient(self, w):

        res = np.dot(np.dot(self.X.T, self.X), w.T) - np.dot(self.X.T, self.t.T)
        assert res.shape[0] == self.d

        return res

    def base_method_error(self, w):
        SSE = 0
        for i in range(0, self.n):
            SSE += (self.t[i] - np.dot(self.X[i, :], w.T)) ** 2
        return SSE / 2

    def base_method_normalization(self):
        #return 1
        return self.n

    def save_model(self, filename, data_file_name):
        os.makedirs(self.get_model_directory() + data_file_name + "/", exist_ok=True)
        np.savez(self.get_model_directory() + data_file_name + "/" + filename, data_file=data_file_name, base_method="linear", w = self.w, w_base = self.w_base, alpha = self.alpha, beta = self.beta, k = self.k, N = self.n, d = self.d)


class BalancedLogisticRegressionClassifier(BalancedClassifier):

    def base_method_solution(self):
        #return np.zeros(self.d)
        logreg = LogisticRegression().fit(self.X[:,1:], self.t)
        return np.reshape(np.append(logreg.intercept_, logreg.coef_), (self.d))

    def base_method_gradient(self, w):
        Ht = np.fromiter((heaviside(self.t[i]) for i in range(0, self.n)), np.float, count=self.n)
        y = np.fromiter((sigmoid(np.dot(self.X[i, :].T, w.T)) for i in range(0, self.n)), np.float, count=self.n)
        res = np.dot(self.X.T, (y - Ht))
        assert res.shape[0] == self.d

        return res

    def base_method_error(self, w):
        CEE = 0
        for i in range(0, self.n):
            y = sigmoid(np.dot(self.X[i, :], w.T))
            t_i = heaviside(self.t[i])
            if (y == 0):
                CEE += (1 - t_i) * math.log(1 - y)
            elif (y == 1):
                CEE += t_i * math.log(y)
            else:
                CEE += t_i * math.log(y) + (1 - t_i) * math.log(1 - y)
        return -CEE

    def base_method_normalization(self):
        #return 1
        return self.n

    def save_model(self, filename, data_file_name):
        os.makedirs(self.get_model_directory() + data_file_name + "/", exist_ok=True)
        np.savez(self.get_model_directory() + data_file_name + "/" + filename, data_file=data_file_name, base_method="logistic", w = self.w, w_base = self.w_base, alpha = self.alpha, beta = self.beta, k = self.k, N = self.n, d = self.d)


def sigmoid(x, k=1):
    try:
        return 1/(1+math.exp(-k*x))
    except OverflowError:
        if -k*x >0:
            return 0
        if -k*x <= 0:
            return 1

def heaviside(x):
    if x == 0:
        return 0.5

    return 0 if x < 0 else 1

