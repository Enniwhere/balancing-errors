import numpy as np

def get_predictive_statistics_decision_boundary(X, t, w):
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    N = t.shape[0]
    d = w.shape[0]
    w = np.reshape(w, (d))

    if X.shape != (N, d):
        X = np.concatenate((np.zeros((N, 1)), X),axis=1)
    assert X.shape == (N, d)

    for i in range(0, N):
        if (np.dot(X[i, :], w.T) >= 0 and t[i] >= 0):
            TP += 1
        elif (np.dot(X[i, :], w.T) >= 0 and t[i] < 0):
            FP += 1
        elif (np.dot(X[i, :], w.T) < 0 and t[i] < 0):
            TN += 1
        elif (np.dot(X[i, :], w.T) < 0 and t[i] >= 0):
            FN += 1
        else:
            print("Measuring error! Neither of the cases worked! Output: " + str(np.dot(X[i, :], w.T)) + " and target: " + str(t[i]))

    assert TP+FP+TN+FN == N
    return TP, FP, TN, FN

def get_FNR_and_FPR(X, t, w):
    TP, FP, TN, FN = get_predictive_statistics_decision_boundary(X, t, w)
    return FN/(TP + FN), FP/(TN + FP)

def get_accuracy(X, t, w):
    TP, FP, TN, FN = get_predictive_statistics_decision_boundary(X, t, w)
    return (TP + TN)/(TP + TN + FP + FN)